# Android Validator

Simplified, flexible and customizable validator. It supports

* Material TextInputLayout
* OnFocusChange and onTextChanged
* Customizable error messages
* Manual error handling through an interface
* Email Validation (based on Android's Email Matcher)
* Phone Number Validation (based on Android's Phone Number Matcher)
* Password Validation (upper char, lower char, digit, special char, min char)
* Confirm Password Validation

## [Demo](https://appetize.io/app/zwj3xzcy2zbtt8c78gq8yzum7g?device=nexus5&scale=75&orientation=portrait&osVersion=6.0)

## Installing (Not Working for now)

1) In your root build.gradle:

```gradle
allprojects {
 repositories {
    jcenter()
    maven { url "https://jitpack.io" }
 }
}
```

2) In your library/build.gradle add:

```gradle
dependencies {
    compile 'com.github.philip-bui:android-validator:1.0.2'
}
```

## Usage
```
  EditText email = (EditText) findViewById(R.id.email);
  EmailValidator emailValidator = new EmailValidator(email);
  emailValidator.setTextInputLayoutValidate(true);
  emailValidator.setRequired(true, "Required email address"); 
  emailValidator.setLoseFocusValidate(true); // Enter some text and change focus and see the magic happen.
  if (emailValidator.validate()) {
   // No error message displayed on screen and successfully validated.
   
  } else {
   // Error message displayed on screen and successfully validated
   
  }
```

## Customization

Customization is easy. Just extend Validator and add your own logic.

```
  public class PhoneNumberValidator extends Validator {
    
    private String errorMessage;
    public PhoneNumberValidator(EditText editText) {
        super(editText);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String onValidate(String string, Context context) {
        if (!Patterns.PHONE.matcher(string).matches()) {
            if (!TextUtils.isEmpty(errorMessage)) {
                return errorMessage;
            }
            return context.getString(R.string.av_phone_number_is_invalid);
        }
        return null;
    }
}
```

This means that you can keep extending your own Validators and reuse code. Just return the appropiate error message 
and the Validator class will handle the error display / callbacks. 

- Returning `""` is considered an error message on purpose, return null.
- To manually handle error messages, set showError(false) and assign a ValidationListener which will pass the success and error messages.
- An onFocusChange listener is attached when lostFocusValidate is set true. If you need additional logic, just override the method and call its super. Otherwise set it to false then attach your onFocusChange listener.

## Customization Attributes

`` validateListener ``

Interface when a success or error validation happens. 
*Default: null*

`` stopTypingValidate ``

Whether to validate when the user stops typing. 
*Default: false*

`` loseFocusValidate ``

Whether to validate when the user changes focus FROM the field.

*Default: true*

`` showError ``

Whether to show error messages. Useful to set false if you want to manually handle error messages.
*Default: true*

`` textInputValidate ``

Whether to show the error message on the parent TextInputLayout. If the parent of the EditText is not a TextInputLayout, it throws an Exception when set to true.
*Default: false*

`` required ``

Whether to return an error when empty. This is called before validate(), so you can also set a custom Error message.
*Default: false, "Required field"*

## About

I made this library as there was a lack of Form Validators that validates with the new Material TextInputLayout or are 
flexible and customizable.

Any problems, feel free to contact me!
